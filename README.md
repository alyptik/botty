# botty

botty is an IRC bot who converts pull requests/issues number into links.

It is based on the testbot included with python's irc package.

Launch it with:

```bash
./botty.py irc.freenode.net:6667 "#neomutt" botty
```

